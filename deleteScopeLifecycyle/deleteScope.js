const couchbase = require('couchbase')
const fs = require('fs');

const QOVERY_OUTPUT_FILE =  "/qovery-output/qovery-output.json";

const endpoint = process.env.DB_ENDPOINT || "couchbase://localhost";
const username = process.env.DB_USERNAME || "Administrator";
const password = process.env.DB_PASSWORD || "Administrator";
const bucket = process.env.DB_BUCKET || "default";
const scope = process.env.QOVERY_ENVIRONNEMENT_ID.slice(0, 10) || "_default";

couchbase.connect(endpoint, {
    username: username,
    password: password,
    configProfile: 'wanDevelopment',
  }
  )
    .then(cluster => {
      const collectionManager = cluster.bucket(bucket).collections();
      collectionManager.dropScope(scope).then(res => {
        console.log("deleted Scope " + scope);
        console.log(scope);
        writeJobOutput();
      }).catch(err => {
        console.log("Cant delete scope");
        console.log(err);
        return err;});
    }
    ).catch(err => {
      console.log("Cant login to cluster");
      console.log(err);
      return err;
    });

    function writeJobOutput(){
      const jobOutput = {
        "DB_SCOPE": {
          "sensitive": false,
          "value": scope
        }
      };
      const data = JSON.stringify(jobOutput);
      fs.writeFileSync(QOVERY_OUTPUT_FILE, data);
    }
    

   
