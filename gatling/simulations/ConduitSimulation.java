
import java.util.*;

import io.gatling.javaapi.core.*;
import io.gatling.javaapi.http.*;

import static io.gatling.javaapi.core.CoreDsl.*;
import static io.gatling.javaapi.http.HttpDsl.*;

public class ConduitSimulation extends Simulation {

  private final String API_ROOT = System.getenv("API_ROOT");

  private HttpProtocolBuilder httpProtocol = http
    .baseUrl(API_ROOT)
    .inferHtmlResources()
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip, deflate, br")
    .acceptLanguageHeader("fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3")
    .userAgentHeader("Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/111.0");
  
  private Map<CharSequence, String> headers_0 = Map.ofEntries(
    Map.entry("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8"),
    Map.entry("Cache-Control", "no-cache"),
    Map.entry("Pragma", "no-cache"),
    Map.entry("Sec-Fetch-Dest", "document"),
    Map.entry("Sec-Fetch-Mode", "navigate"),
    Map.entry("Sec-Fetch-Site", "cross-site"),
    Map.entry("Upgrade-Insecure-Requests", "1")
  );
  
  private Map<CharSequence, String> headers_1 = Map.ofEntries(
    Map.entry("Cache-Control", "no-cache"),
    Map.entry("Pragma", "no-cache"),
    Map.entry("Sec-Fetch-Dest", "script"),
    Map.entry("Sec-Fetch-Mode", "no-cors"),
    Map.entry("Sec-Fetch-Site", "same-origin")
  );
  
  private Map<CharSequence, String> headers_2 = Map.ofEntries(
    Map.entry("Accept", "text/css,*/*;q=0.1"),
    Map.entry("Cache-Control", "no-cache"),
    Map.entry("Pragma", "no-cache"),
    Map.entry("Sec-Fetch-Dest", "style"),
    Map.entry("Sec-Fetch-Mode", "no-cors"),
    Map.entry("Sec-Fetch-Site", "cross-site")
  );
  
  private Map<CharSequence, String> headers_3 = Map.ofEntries(
    Map.entry("Cache-Control", "no-cache"),
    Map.entry("Pragma", "no-cache"),
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin")
  );
  
  private Map<CharSequence, String> headers_5 = Map.ofEntries(
    Map.entry("Accept", "application/font-woff2;q=1.0,application/font-woff;q=0.9,*/*;q=0.8"),
    Map.entry("Accept-Encoding", "identity"),
    Map.entry("Cache-Control", "no-cache"),
    Map.entry("Origin", API_ROOT),
    Map.entry("Pragma", "no-cache")
  );
  
  private Map<CharSequence, String> headers_10 = Map.ofEntries(
    Map.entry("Accept", "application/font-woff2;q=1.0,application/font-woff;q=0.9,*/*;q=0.8"),
    Map.entry("Cache-Control", "no-cache"),
    Map.entry("Origin", API_ROOT),
    Map.entry("Pragma", "no-cache"),
    Map.entry("Sec-Fetch-Dest", "font"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "cross-site")
  );
  
  private Map<CharSequence, String> headers_11 = Map.ofEntries(
    Map.entry("Content-Type", "application/json"),
    Map.entry("Origin", API_ROOT),
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin")
  );
  
  private Map<CharSequence, String> headers_12 = Map.ofEntries(
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin"),
    Map.entry("authorization", "Token ${accessToken}")
  );
  
  
  private Map<CharSequence, String> headers_14 = Map.ofEntries(
    Map.entry("Content-Type", "application/json"),
    Map.entry("Origin", API_ROOT),
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin"),
    Map.entry("authorization", "Token ${accessToken}")
  );
  
  private Map<CharSequence, String> headers_15 = Map.ofEntries(
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin"),
    Map.entry("authorization", "Token ${accessToken}")
  );
  
  private Map<CharSequence, String> headers_17 = Map.ofEntries(
    Map.entry("Accept", "application/font-woff2;q=1.0,application/font-woff;q=0.9,*/*;q=0.8"),
    Map.entry("Accept-Encoding", "identity"),
    Map.entry("Origin", API_ROOT)
  );
  
  private Map<CharSequence, String> headers_20 = Map.ofEntries(
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin"),
    Map.entry("authorization", "Token ${accessToken}")
  );
  
  private Map<CharSequence, String> headers_27 = Map.ofEntries(
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin"),
    Map.entry("authorization", "Token ${accessToken}")
  );
  
  private Map<CharSequence, String> headers_30 = Map.ofEntries(
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin"),
    Map.entry("authorization", "Token ${accessToken}")
  );
  
  private Map<CharSequence, String> headers_32 = Map.ofEntries(
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin"),
    Map.entry("authorization", "Token ${accessToken}")
  );
  
  private Map<CharSequence, String> headers_34 = Map.ofEntries(
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin")
  );
  
  private Map<CharSequence, String> headers_35 = Map.ofEntries(
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin")
  );
  
  private Map<CharSequence, String> headers_39 = Map.ofEntries(
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin"),
    Map.entry("authorization", "Token ${accessToken}")
  );
  
  private Map<CharSequence, String> headers_40 = Map.ofEntries(
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin"),
    Map.entry("authorization", "Token ${accessToken}")
  );
  
  private Map<CharSequence, String> headers_42 = Map.ofEntries(
    Map.entry("Origin", API_ROOT),
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin"),
    Map.entry("authorization", "Token ${accessToken}")
  );
  
  private Map<CharSequence, String> headers_44 = Map.ofEntries(
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin"),
    Map.entry("authorization", "Token ${accessToken}")
  );
  
  private Map<CharSequence, String> headers_45 = Map.ofEntries(
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin"),
    Map.entry("authorization", "Token ${accessToken}")
  );
  
  private Map<CharSequence, String> headers_54 = Map.ofEntries(
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin"),
    Map.entry("authorization", "Token ${accessToken}")
  );
  
  private Map<CharSequence, String> headers_58 = Map.ofEntries(
    Map.entry("Sec-Fetch-Dest", "empty"),
    Map.entry("Sec-Fetch-Mode", "cors"),
    Map.entry("Sec-Fetch-Site", "same-origin")
  );
  
  private final String uri2 = "https://code.ionicframework.com/ionicons/2.0.1";
  
  private final String uri3 = "http://fonts.gstatic.com/s";

  private HttpRequestActionBuilder loadHomePage(){
      return http(String.format("request_0:GET_%s/", API_ROOT))
        .get("/")
        .headers(headers_0)
        .resources(
          http(String.format("request_1:GET_%s/src/index.js", API_ROOT))
            .get("/src/index.js")
            .headers(headers_1)
            .check(status().is(404)),
          http(String.format("request_2:GET_%s/css/ionicons.min.css", uri2))
            .get(uri2 + "/css/ionicons.min.css")
            .headers(headers_2),
          http(String.format("request_3:GET_%s/api/tags", API_ROOT))
            .get("/api/tags")
            .headers(headers_3),
          http(String.format("request_4:GET_%s/api/articles?limit=10&offset=0", API_ROOT))
            .get("/api/articles?limit=10&offset=0")
            .headers(headers_3),
          http(String.format("request_5:GET_%s/sourcesanspro/v21/6xK3dSBYKcSV-LCoeQqfX1RYOo3qOK7l.woff2", uri3))
            .get(uri3 + "/sourcesanspro/v21/6xK3dSBYKcSV-LCoeQqfX1RYOo3qOK7l.woff2")
            .headers(headers_5),
          http(String.format("request_6:GET_%s/sourcesanspro/v21/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwlxdu.woff2", uri3))
            .get(uri3 + "/sourcesanspro/v21/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwlxdu.woff2")
            .headers(headers_5),
          http(String.format("request_7:GET_%s/titilliumweb/v15/NaPDcZTIAOhVxoMyOr9n_E7ffHjDGItzYw.woff2", uri3))
            .get(uri3 + "/titilliumweb/v15/NaPDcZTIAOhVxoMyOr9n_E7ffHjDGItzYw.woff2")
            .headers(headers_5),
          http(String.format("request_8:GET_%s/sourcesanspro/v21/6xKydSBYKcSV-LCoeQqfX1RYOo3i54rwlxdu.woff2", uri3))
            .get(uri3 + "/sourcesanspro/v21/6xKydSBYKcSV-LCoeQqfX1RYOo3i54rwlxdu.woff2")
            .headers(headers_5),
          http(String.format("request_9:GET_%s/sourcesanspro/v21/6xK1dSBYKcSV-LCoeQqfX1RYOo3qPZ7nsDI.woff2", uri3))
            .get(uri3 + "/sourcesanspro/v21/6xK1dSBYKcSV-LCoeQqfX1RYOo3qPZ7nsDI.woff2")
            .headers(headers_5),
          http(String.format("request_10:GET_%s/fonts/ionicons.ttf?v=2.0.1", uri2))
            .get(uri2 + "/fonts/ionicons.ttf?v=2.0.1")
            .headers(headers_10)
        );
  }


  private HttpRequestActionBuilder Signup(String userBody) {
    return http(String.format("request_11:POST_%s/api/users", API_ROOT))
            .post("/api/users")
            .headers(headers_11)
            .body(RawFileBody(userBody))
            .resources(
                    http(String.format("request_12:GET_%s/api/tags", API_ROOT))
                            .get("/api/tags")
                            .headers(headers_12),
                    http(String.format("request_13:GET_%s/api/articles/feed?limit=10&offset=0", API_ROOT))
                            .get("/api/articles/feed?limit=10&offset=0")
                            .headers(headers_12)
            )
            .asJson().check(status().is(201)).check(jsonPath("$.user.token").saveAs("accessToken"));
  }

  private ScenarioBuilder scn = scenario("Conduit Simulation")
  // Load Home page no user/token
    .exec(
      loadHomePage()
    )
    .pause(2)
    // Signup
    .exec(
            Signup("conduitsimluation/0011_request.json")
    )
    .pause(2)
    // create article
    .exec(
            createArticle("conduitsimluation/0014_request.json")
    )
    .pause(2)
    // Post Comment
    .exec(
            postComment("test-titre", "conduitsimluation/0018_request.json")
    )
    .pause(2)
    // Load article
    .exec(
            loadArticle("test-titre")
    )
    .pause(2)
    // Edit article
    .exec(
            editArticle("test-titre", "conduitsimluation/0021_request.json")
    )
    .pause(5)
    // Fetch Feed
    .exec(
            fetchFeed()
    )
    .pause(2)
    .exec(
      // Get Specific tag
            articlesPerTagAuthenticated("testtag_C3_A9", 10, 0)
    )
    .pause(2)
    // get articles
    .exec(
            getArticlesAuthenticated()
    )
    .pause(1)
    // Get Feed Article
    .exec(
            getUserFeed()
    )
    .pause(2)
    // Edit user
    .exec(
            editUser()
    )
    .pause(2)
    // visit profile
    .exec(
            visitUser()
    )
    .pause(2)
    // Fetch Tags
    .exec(
            fetchArticleUnauthenticated()
    )
    .pause(2)
    // Post User
    .exec(
            Signup("conduitsimluation/0036_request.json")
    )
    .pause(2)
    // Get articles
    .exec(
      http(String.format("request_39:GET_%s/api/articles?limit=10&offset=0", API_ROOT))
        .get("/api/articles?limit=10&offset=0")
        .headers(headers_39)
    )
    .pause(2)
    // visit user profile
    .exec(
            visitUserProfile()
    )
    .pause(1)
    // follow user
    .exec(
            followUser()
    )
    .pause(1)
    // get user favorites
    .exec(
            getUserFavoritedArticles()
    )
    .pause(1)
    // GEt user feed
    .exec(
            getUserFeed2()
    )
    .pause(1)
    // get User Feed
    .exec(
            getFeed()
    )
    .pause(1)
    // get articles
    .exec(
            getArticleHome()
    )
    .pause(1)
    // Favor articles
    .exec(
            favorAritcle("test-titre")
    )
    .pause(1)
    // favor article
    .exec(
            favorAritcle("feq")
    )
    .pause(2)
    // get user feed
    .exec(
      http(String.format("request_52:GET_%s/api/profiles/test3", API_ROOT))
        .get("/api/profiles/test3")
        .headers(headers_44)
        .resources(
          http(String.format("request_53:GET_%s/api/articles?author=test3&limit=5&offset=0", API_ROOT))
            .get("/api/articles?author=test3&limit=5&offset=0")
            .headers(headers_44)
        )
    )
    .pause(1)
    // get user favorites
    .exec(
      http(String.format("request_54:GET_%s/api/profiles/test3", API_ROOT))
        .get("/api/profiles/test3")
        .headers(headers_54)
        .resources(
          http(String.format("request_55:GET_%s/api/articles?favorited=test3&limit=5&offset=0", API_ROOT))
            .get("/api/articles?favorited=test3&limit=5&offset=0")
            .headers(headers_44)
        )
    )
    .pause(2)
    // unfav
    .exec(
            unfav("test-titre")
    )
    .pause(2)
    // list article
    .exec(
      http(String.format("request_57:GET_%s/api/tags", API_ROOT))
        .get("/api/tags")
        .headers(headers_34)
        .resources(
          http(String.format("request_58:GET_%s/api/articles?limit=10&offset=0", API_ROOT))
            .get("/api/articles?limit=10&offset=0")
            .headers(headers_58)
        )
    )
  ;

  private HttpRequestActionBuilder unfav(String slug) {
    return http(String.format("request_56:DELETE_%s/api/articles/test-titre/favorite", API_ROOT))
            .delete("/api/articles/test-titre/favorite")
            .headers(headers_42);
  }

  private HttpRequestActionBuilder favorAritcle(String slug) {
    return http(String.format("request_50:POST_%s/api/articles/test-titre/favorite", API_ROOT))
            .post("/api/articles/test-titre/favorite")
            .headers(headers_42);
  }

  private HttpRequestActionBuilder getArticleHome() {
    return http(String.format("request_49:GET_%s/api/articles?limit=10&offset=0", API_ROOT))
            .get("/api/articles?limit=10&offset=0")
            .headers(headers_39);
  }

  private HttpRequestActionBuilder getFeed() {
    return http(String.format("request_47:GET_%s/api/tags", API_ROOT))
            .get("/api/tags")
            .headers(headers_12)
            .resources(
                    http(String.format("request_48:GET_%s/api/articles/feed?limit=10&offset=0", API_ROOT))
                            .get("/api/articles/feed?limit=10&offset=0")
                            .headers(headers_12)
            );
  }

  private HttpRequestActionBuilder getUserFeed2() {
    return http(String.format("request_45:GET_%s/api/profiles/test", API_ROOT))
            .get("/api/profiles/test")
            .headers(headers_45)
            .resources(
                    http(String.format("request_46:GET_%s/api/articles?author=test&limit=5&offset=0", API_ROOT))
                            .get("/api/articles?author=test&limit=5&offset=0")
                            .headers(headers_12)
            );
  }

  private HttpRequestActionBuilder getUserFavoritedArticles() {
    return http(String.format("request_43:GET_%s/api/profiles/test", API_ROOT))
            .get("/api/profiles/test")
            .headers(headers_40)
            .resources(
                    http(String.format("request_44:GET_%s/api/articles?favorited=test&limit=5&offset=0", API_ROOT))
                            .get("/api/articles?favorited=test&limit=5&offset=0")
                            .headers(headers_44)
            );
  }

  private HttpRequestActionBuilder followUser() {
    return http(String.format("request_42:POST_%s/api/profiles/test/follow", API_ROOT))
            .post("/api/profiles/test/follow")
            .headers(headers_42);
  }

  private HttpRequestActionBuilder visitUserProfile() {
    return http(String.format("request_40:GET_%s/api/profiles/test", API_ROOT))
            .get("/api/profiles/test")
            .headers(headers_40)
            .resources(
                    http(String.format("request_41:GET_%s/api/articles?author=test&limit=5&offset=0", API_ROOT))
                            .get("/api/articles?author=test&limit=5&offset=0")
                            .headers(headers_12)
            );
  }

  private HttpRequestActionBuilder fetchArticleUnauthenticated() {
    return http(String.format("request_34:GET_%s/api/tags", API_ROOT))
            .get("/api/tags")
            .headers(headers_34)
            .resources(
                    http(String.format("request_35:GET_%s/api/articles?limit=10&offset=0", API_ROOT))
                            .get("/api/articles?limit=10&offset=0")
                            .headers(headers_35)
            );
  }

  private HttpRequestActionBuilder visitUser() {
    return http(String.format("request_32:GET_%s/api/profiles/test", API_ROOT))
            .get("/api/profiles/test")
            .headers(headers_32)
            .resources(
                    http(String.format("request_33:GET_%s/api/articles?author=test&limit=5&offset=0", API_ROOT))
                            .get("/api/articles?author=test&limit=5&offset=0")
                            .headers(headers_12)
            );
  }

  private HttpRequestActionBuilder editUser() {
    return http(String.format("request_29:PUT_%s/api/user", API_ROOT))
            .put("/api/user")
            .headers(headers_14)
            .body(RawFileBody("conduitsimluation/0029_request.json"))
            .resources(
                    http(String.format("request_30:GET_%s/api/tags", API_ROOT))
                            .get("/api/tags")
                            .headers(headers_30),
                    http(String.format("request_31:GET_%s/api/articles/feed?limit=10&offset=0", API_ROOT))
                            .get("/api/articles/feed?limit=10&offset=0")
                            .headers(headers_12)
            );
  }

  private HttpRequestActionBuilder getUserFeed() {
    return http(String.format("request_28:GET_%s/api/articles/feed?limit=10&offset=0", API_ROOT))
            .get("/api/articles/feed?limit=10&offset=0")
            .headers(headers_12);
  }

  private HttpRequestActionBuilder getArticlesAuthenticated() {
    return http(String.format("request_27:GET_%s/api/articles?limit=10&offset=0", API_ROOT))
            .get("/api/articles?limit=10&offset=0")
            .headers(headers_27);
  }

  private HttpRequestActionBuilder articlesPerTagAuthenticated(String tag, int limit, int offset ) {
    return http(String.format("request_26:GET_%s/api/articles?tag=%s&limit=%d&offset=%d", API_ROOT, tag, limit, offset))
            .get(String.format("/api/articles?tag=%s&limit=%d&offset=%d",tag, limit, offset))
            .headers(headers_15);
  }

  private HttpRequestActionBuilder fetchFeed() {
    return http(String.format("request_24:GET_%s/api/tags", API_ROOT))
            .get("/api/tags")
            .headers(headers_12)
            .resources(
                    http(String.format("request_25:GET_%s/api/articles/feed?limit=10&offset=0", API_ROOT))
                            .get("/api/articles/feed?limit=10&offset=0")
                            .headers(headers_12)
            );
  }

  private HttpRequestActionBuilder editArticle(String slug, String requestBody) {
    return http(String.format("request_21:PUT_%s/api/articles/%s", API_ROOT, slug))
            .put(String.format("/api/articles/%s", slug))
            .headers(headers_14)
            .body(RawFileBody(requestBody))
            .resources(
                    http(String.format("request_22:GET_%s/api/articles/%s", API_ROOT, slug))
                            .get(String.format("/api/articles/%s", slug))
                            .headers(headers_15),
                    http(String.format("request_23:GET_%s/api/articles/%s/comments", API_ROOT, slug))
                            .get(String.format("/api/articles/%s/comments", slug))
                            .headers(headers_15)
            );
  }

  private HttpRequestActionBuilder loadArticle(String slug) {
    return http(String.format("request_20:GET_%s/api/articles/%s", API_ROOT, slug))
            .get(String.format("/api/articles/%s", slug))
            .headers(headers_20);
  }

  private HttpRequestActionBuilder postComment(String slug, String commentBodyRequest) {
    return http(String.format("request_18:POST_%s/api/articles/%s/comments", API_ROOT, slug))
            .post(String.format("/api/articles/%s/comments", slug))
            .headers(headers_14)
            .body(RawFileBody(commentBodyRequest))
            .resources(
                    http(String.format("request_19:GET_%s/sourcesanspro/v21/6xKwdSBYKcSV-LCoeQqfX1RYOo3qPZZMkids18Q.woff2", API_ROOT))
                            .get(uri3 + "/sourcesanspro/v21/6xKwdSBYKcSV-LCoeQqfX1RYOo3qPZZMkids18Q.woff2")
                            .headers(headers_17)
            );
  }

  private HttpRequestActionBuilder createArticle(String bodyReq ) {
    return http(String.format("request_14:POST_%s/api/articles", API_ROOT))
            .post("/api/articles")
            .headers(headers_14)
            .body(RawFileBody(bodyReq))
            // TODO: Save article slug
            .resources(
                    http(String.format("request_15:GET_%s/api/articles/test-titre", API_ROOT))
                            .get("/api/articles/test-titre")
                            .headers(headers_15),
                    http(String.format("request_16:GET_%s/api/articles/test-titre/comments", API_ROOT))
                            .get("/api/articles/test-titre/comments")
                            .headers(headers_15),
                    http(String.format("request_17:GET_%s/sourcesanspro/v21/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwlxdu.woff2", API_ROOT))
                            .get(uri3 + "/sourcesanspro/v21/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwlxdu.woff2")
                            .headers(headers_17)
            );
  }

  {
    if (API_ROOT == null) {
      throw new RuntimeException("API_ROOT environment variable must be specified");
    }
	  setUp(scn.injectOpen(atOnceUsers(10))).protocols(httpProtocol);
  }
}
