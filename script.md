# Running the e2e test

## Configuration

For everything to work, you will need the following env variable setup
```
# configure the database access
export DB_ENDPOINT='couchbases://cb.c8bevy9j8nkiej.cloud.couchbase.com'
export DB_USERNAME='ldoguin'
export DB_PASSWORD='XXXXXXXXXXXXXX!'
export DB_BUCKET='default'
export DB_SCOPE='_default'
# Configure the test, test also need to run with database env
export API_ROOT='http://localhost:4000/'
```

## Start with the all in done docker container

```
docker build -t everything .
docker run --name everything -e DB_PASSWORD='yourPassword!' -e DB_BUCKET='default' -e DB_SCOPE='_default' -e DB_ENDPOINT='couchbases://cb.c8bevy9j8nkiej.cloud.couchbase.com' -e DB_USERNAME='username'  -p 4000:4000 everything
```

## Start the test

`npm install`
`npm run "cypress:run"`

make sure all env variable are set appropriately.


## Reference

### Start the database locally

`docker run -d --name db -p 8091-8097:8091-8097 -p 9123:9123 -p 11207:11207 -p 11210:11210 -p 11280:11280 -p 18091-18097:18091-18097 couchbase`

Than setup the cluster on `http://localhost:8091`. Make sure you have a default bucket and the data,query and index services activated. The other are optionals.

### Start the server

`cd server && node api/index.js`

### Start the client

#### Build
Build with `cd client && npm run build`. If you get an error you might need this option for the build `export NODE_OPTIONS=--openssl-legacy-provider`

#### start

`cd client/build && npx http-server --port 4100`

### Export/Import Scope

#### Export scope

sudo docker run -it -v ./output:/output --network=host couchbase cbexport json -c $DB_ENDPOINT -u $DB_USERNAME -p $DB_PASSWORD -b $DB_BUCKET -o ./output/list.json -f list --scope-field _scope --collection-field  _collection --include-key _key --include-data $DB_SCOPE

#### import the export

sudo docker run -it -v ./output:/output --network=host couchbase cbimport json -c $DB_ENDPOINT -u $DB_USERNAME -p $DB_PASSWORD -b $DB_BUCKET -d file:///output/list.json -f list  -g %_key% --scope-collection-exp %_scope%.%_collection%
